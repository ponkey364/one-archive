/*  This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

export type vCard = {
	name: string;
	formattedName: string;
	nickname?: string;
	tel?: Array<string>;
	email?: Array<string>;
	impp?: Array<{
		[key: string]: {
			dest: "HOME" | "WORK" | "MOBILE" | string;
			addr: string;
		};
	}>;
};

export type vCardContacts = Array<vCard>;

export function parse(data: string) {
	let splitData = data.split(
			/(?:END\:VCARD)(?:\r\n|\r|\n)(?:BEGIN\:VCARD)(?:\r\n|\r|\n)/)

	let rawCards = splitData.map(card =>
		card.split(/(\r\n|\r|\n)/).filter(
			field =>
				!(
					field.startsWith("VERSION") ||
					field.startsWith("PRODID") ||
					field.startsWith("BEGIN:VCARD") ||
					field.startsWith("END:VCARD") ||
					field.startsWith("PHOTO") ||
					field.startsWith(" ") || // Single space seems to signify a continuation of image data ¯\_(ツ)_/¯
					field.startsWith("ORG") ||
					field.startsWith("TITLE") ||
					field.startsWith("BDAY") ||
					field == ""
				)
		)
	);

	// Split all of the fields into their individual components & remove empty str
	// Then we can split at : to separate property names & their values
	let splitCards = rawCards.map(card =>
		card.map(field =>
			field
				.split(";")
				.filter(el => el !== "")
				.flatMap(s => s.split(":"))
		)
	);

	let cards = splitCards.map(card => {
		let jsCard: vCard = { name: "", formattedName: "", tel: [], email: [] };

		card.forEach(field => {
			switch (field[0]) {
				case "N": {
					// Name
					jsCard.name = field
						.slice(1)
						.reverse()
						.join(" ");
					break;
				}
				case "FN": {
					// Formatted Name
					jsCard.formattedName = field[1];
					break;
				}
				case "TEL": {
					// Phone Number (duh)
					jsCard.tel?.push(
						field
							.slice(1)
							.filter(v => !v.startsWith("type="))[0]
							.split(" ")
							.join("") // Split and re-join to remove spaces
					);
					break;
				}
				case "EMAIL": {
					jsCard.email?.push(
						field.slice(1).filter(v => !v.startsWith("type="))[0]
					);
					break;
				}
				default: {
					if (field[0].endsWith("EMAIL")) {
						// We've been apple'd
						jsCard.email?.push(
							field.slice(1).filter(v => !v.startsWith("type="))[0]
						);
					}
					break;
				}
			}
		});

		return jsCard;
	});

	return cards;
}
