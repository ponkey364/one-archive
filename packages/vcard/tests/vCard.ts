import ava from "ava";
import { parse } from "../dist/index";

ava("CLI: vCard - parser", t => {
	let vCardData = `BEGIN:VCARD\nVERSION:3.0\nPRODID:-//Apple Inc.//Mac OS X 10.15//EN\nN:User;Test;;;\nFN:Test User\nTEL;type=HOME;type=VOICE;type=pref:555555555\nEMAIL;type=INTERNET;type=HOME;type=pref:testuser@rpgpn.co.uk\nEND:VCARD\n`;
	let expected = [
		{
			name: "Test User",
			formattedName: "Test User",
			tel: ["555555555"],
			email: ["testuser@rpgpn.co.uk"]
		}
	];

	t.deepEqual(parse(vCardData), expected);
});
