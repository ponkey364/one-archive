/*  This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

import inquirer from "inquirer";
import { Answers as AnswersType } from "./Types";

let prompts = [
	{
		type: "list",
		message: "What chat platform would you like to archive",
		name: "platform",
		choices: [
			{ name: "Hangouts", value: "gho" },
			{ name: "iMessage (Incl. SMS)", value: "imsg" },
			{ name: "SMS Backup & Restore (signal-back compatible)", value: "smsbr" }
		]
	},
	{
		type: "list",
		message: "Where should I load the chat database from?",
		name: "iMsg_location",
		choices: [
			{ name: "Load from default location", value: "default" },
			{ name: "Load from other location", value: "other" }
		],
		when: (ans: any) => ans.platform == "imsg" && process.platform == "darwin"
	},
	{
		type: "input",
		message: "File location",
		name: "file",
		when: (ans: any) =>
			(process.platform == "darwin" && ans.iMsg_location == "other") ||
			(process.platform !== "darwin" && ans.platform == "imsg") ||
			ans.platform == "smsbr",
		validate: (val: string) => val.endsWith(".xml")
	},
	{
		type: "list",
		message: "How should I separate messages from different people",
		name: "separate",
		choices: [
			{ name: "Split into multiple OA files", value: "split" },
			{ name: "Keep as one OA Library file", value: "library" }
		]
	},
	{
		type: "confirm",
		message: "Should I parse a vCard contacts file to apply names to chatters?",
		name: "vcard"
	},
	{
		type: "input",
		message: "vCard location",
		name: "vcard_location",
		when: (ans: AnswersType) => ans.vcard
	}
];

inquirer
	.prompt(prompts)
	.then((answers: AnswersType) => {
		console.dir(answers);
		switch (answers.platform) {
			case "gho": {
				break;
			}
			case "imsg": {
				break;
			}
			case "smsbr": {
				break;
			}
		}
	})
	.catch(err => {
		throw new Error(err);
	});
