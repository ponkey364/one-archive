export interface Message {
	body: string;
	date: Date;
	conversationWith: Array<string>;
	direction: "in" | "out";
	friendlyName?: string;
	sender: string;
}

export type MessageTree = {
	[key: string]: Array<Message>;
};