/*  This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

import protobuf from 'protobufjs';

const PROTO = {
	nested: {
		onearchive: {
			nested: {
				ChatMessage: {
					fields: {
						msg: { type: "string", id: 1 },
						dir: { type: "Dir", id: 2 },
						sender: { type: "string", id: 3 },
						date: { type: "string", id: 4 }
					},
					nested: { Dir: { values: { OUT: 0, IN: 1 } } }
				}
			}
		}
	}
};

export default PROTO

export const root = protobuf.Root.fromJSON(PROTO)

export const ChatMessage = root.lookupType("onearchive.ChatMessage")