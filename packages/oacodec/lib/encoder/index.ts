/* 
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

import { MessageTree } from "@oa/types";
import { vCard, vCardContacts } from "@oa/vcard";
import { ChatMessage } from "../proto";
import zlib from "zlib";

interface TableOfContents {
	[key: string]: Array<{ contact: vCard; length: number }>;
}

export default function(
	tree: MessageTree,
	vCards: vCardContacts,
	splitOnMulti: boolean,
	encryption?: { key: string; iv: string }
) {
	if (Object.keys(tree).length > 1) {
		// Multiple
	} else {
		// Only 1, luckily
		let contact = Object.keys(tree)[0];

		let f = vCards.filter(
			card => card.tel?.includes(contact) || card.email?.includes(contact)
		);

		if (f.length == 0) {
			// No vCard for the contact
		}

		let buffers: Array<Uint8Array | Buffer> = [];

		tree[contact].forEach(msg => {
			let payload = {
				msg: msg.body,
				dir: msg.direction.toUpperCase(),
				sender: msg.sender,
				date: msg.date.toString()
			};
			buffers.push(ChatMessage.encode(payload).finish());
		});

		let dataBlock = Buffer.concat(buffers);

		let compressed = zlib.deflateSync(dataBlock);

		return Buffer.concat([prepareHeader({}, false), compressed]);
	}
}

export function convertTreeToBuffer(tree: MessageTree) {}

function convertTocToString(toc: TableOfContents) {}

function prepareHeader(
	toc: TableOfContents,
	encScheme: "AES" | false,
	iv?: string,
) {
	return Buffer.from(
		`ONEARCHIVE-DATA?lastWrite=${new Date().getTime()};compression=gzip;encryption=${encScheme ? encScheme : 0},${encScheme ? iv : 0}
ONEARCHIVE-TOC=${convertTocToString(toc)}\n`
	);
}
