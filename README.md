# OneArchive

Archiving utility for chat messages

## Packages

- [CLI](https://gitlab.com/ponkey364/one-archive/tree/master/packages/cli)
- [vCard Parser](https://gitlab.com/ponkey364/one-archive/tree/master/packages/vcard)
- [Encoder / Decoder](https://gitlab.com/ponkey364/one-archive/tree/master/packages/oacodec)
- [GoogleTalk (Hangouts) Grabber & Parser](https://gitlab.com/ponkey364/one-archive/tree/master/packages/hangouts)
- [Apple iMessage Parser](https://gitlab.com/ponkey364/one-archive/tree/master/packages/fruitsql)
- ~~SMS Backup & Sync Parser~~ (**COMING SOON!**)
- [Typescript typings](https://gitlab.com/ponkey364/one-archive/tree/master/packages/types)


## Implementation Progress

* CLI
    - [x] Basic Questions
    - [ ] Pass data through to converters
* Hangouts **NOT YET IMPLEMENTED**
* FruitSQL **NOT YET IMPLEMENTED**
* Encoder / Decoder
    - [x] Protobuf messages
    * Encoder
        - [x] Pipe messages through protobuf
        - [x] Compression
        - [ ] AES Encryption
    * Decoder
        - [ ] Load File
        - [ ] Decompress
        - [ ] Decryption
* vCard Parser **COMPLETE**
* Typings **COMPLETE**